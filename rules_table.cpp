/*
    BIN projekt - xhrade08
*/


#include <iostream>
#include <fstream>               // na soubory  
#include <string>                // std::stoi
#include <sstream>

using namespace std;

void load_langton_tables(int *langston_table)
{
    fstream fin;
    fin.open("Langtons-Loops.table", ios::in);

    if (! fin.is_open())
    {
        cerr << "Nelze otevrit soubor Langtons-Loops.table" << endl;
        exit(1);
    }


    string line;
    while(getline(fin,line))
    {
        if (line[0]=='#') continue;

        int new_center;
        if ( ! (istringstream(line.substr(5,1)) >> new_center) ) continue;

        //format: CNESWC'

        // vezmeme cislo v radku jako osmickove a prevedeme jej do desitkove soustavy
        // adesujeme pak timto cislem a na jeho pozici vlozime posledni cislici
        for (int i=0; i<4; i++)
        {
            int address = std::stoi (line.substr(0,5),nullptr,8);
            langston_table[address] = new_center;
            //cout << line << endl;
            line.insert (1,line.substr(4,1));
            
        }
        //cout << endl;

        //cout << "adresa " << address << " centr " << new_center << endl;

        if(!fin.good()) break;

    }

    fin.close();
}