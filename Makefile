# Autor: Michal Hradecky, xhrade08@stud.fit.vutbr.cz
# BIN projekt - semulace CA na GPu

# Makefile pro GPU verzi simulace

CPP          = g++
CPPFLAGS      = -std=c++0x -pedantic -Wextra -g -O3

#CUDA
NVCC = nvcc
NVCC_PARAMS = -g -G -m64  -lineinfo -gencode arch=compute_20,code=sm_20 
# --ptxas-options="-v" - bude vypisovat dalsi info o prekladu
# --ptxas-options="-dlcm=cg" - necachovane nacitani
#NVCC_PARAMS = -m64  -lineinfo -gencode arch=compute_20,code=sm_20 --ptxas-options="-v -dlcm=cg"
#NVCC_PARAMS = -m64  -lineinfo -gencode arch=compute_20,code=sm_20 --ptxas-options="-v"
CUDA_INCLUDE = -I/usr/local/cuda/include -I. -I.. -I../../common/inc
CUDA_LIBS = -m64 -L/usr/local/cuda/lib64 -lcudart


all: ca_gpu

ca_gpu: ca_gpu.o rules_table.o
	$(CPP) $(CPPFLAGS) -o ca_gpu ca_gpu.o rules_table.o $(CUDA_LIBS)

ca_gpu.o: ca_gpu.cu rules_table.cpp rules_table.h
	$(NVCC) $(NVCC_PARAMS) $(CUDA_INCLUDE) -c ca_gpu.cu
	
rules_table.o: rules_table.cpp rules_table.h	
	$(CPP) $(CPPFLAGS) -c rules_table.cpp

clean:
	rm *.o ca_gpu

pack: 
	zip xhrade08.zip Makefile *.cpp *.h *.cu
