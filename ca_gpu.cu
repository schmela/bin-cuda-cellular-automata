#include <stdio.h>
#include <cstdlib>
#include <iostream>

#include <cuda_profiler_api.h>   // na mereni casu

#include "rules_table.h"

using namespace std;

#define DIM_BLOCK_X 32	// delka jednoho bloku
#define DIM_BLOCK_Y 32

#define LANGTON_TABLE_SIZE 32768

// parametry k meneni ----------------------------------------------------------------
const int	ITERATIONS = 10000;		// pocet iteraci

#define SIZE_X 32*DIM_BLOCK_X       // delka celeho gridu
#define SIZE_Y 32*DIM_BLOCK_Y	

// konec parametru k meneni---------------------------------------------------------------

surface<void, cudaSurfaceType2D> surface_ref;    // na uchovavani hodnot iteraci
surface<void, cudaSurfaceType2D> surface_ref_2;

texture<int, 1, cudaReadModeElementType> tex_langton_ref;	// na pravidla pro langtona

bool langton=false;
bool convey=false;


/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// kernel na spocteni 1 iterace hry life

__global__ void kernel_life_2d_surface ()
{
	const int x = (blockIdx.x * blockDim.x) + threadIdx.x;	//calculate the adress from all the block/thread ids
	const int y = (blockIdx.y * blockDim.y) + threadIdx.y;
		
	int tmp, soucet=0;

	surf2Dread(&tmp, surface_ref, (x)*sizeof(int), y-1, cudaBoundaryModeZero);		// nahore
	soucet = tmp;
	surf2Dread(&tmp, surface_ref, (x+1)*sizeof(int), y-1, cudaBoundaryModeZero);    // vpravo nahore
	soucet += tmp;
	surf2Dread(&tmp, surface_ref, (x-1)*sizeof(int), y-1, cudaBoundaryModeZero);    // vlevo nahore
	soucet += tmp;

	surf2Dread(&tmp, surface_ref, (x-1)*sizeof(int), y, cudaBoundaryModeZero);      // vlevo
	soucet += tmp;
	surf2Dread(&tmp, surface_ref, (x+1)*sizeof(int), y, cudaBoundaryModeZero);      // vpravo
	soucet += tmp;
	
	surf2Dread(&tmp, surface_ref, (x)*sizeof(int), y+1, cudaBoundaryModeZero);		// dole
	soucet += tmp;
	surf2Dread(&tmp, surface_ref, (x+1)*sizeof(int), y+1, cudaBoundaryModeZero);    // vpravo dole
	soucet += tmp;
	surf2Dread(&tmp, surface_ref, (x-1)*sizeof(int), y+1, cudaBoundaryModeZero);    // vlevo dole
	soucet += tmp;
	
	surf2Dread(&tmp, surface_ref, (x)*sizeof(int), y, cudaBoundaryModeZero);       // stred


	// dela presne to samy jako zakomentovanej kod dole
	int out = (tmp & ((soucet==3)||(soucet==2))) || (!tmp & (soucet==3));
	surf2Dwrite(out, surface_ref_2, x*sizeof(int), y);
    
    /*
	if (tmp == 1)
    {
        if ((soucet <= 1) || (soucet >= 4)) 
        {
        	surf2Dwrite(0, surface_ref_2, x*sizeof(int), y);
        }
        else
        {
        	surf2Dwrite(1, surface_ref_2, x*sizeof(int), y);
        }
    }
    else if (tmp==0)
    {
        if (soucet == 3)
        {
            surf2Dwrite(1, surface_ref_2, x*sizeof(int), y);
        }
        else
        {
            surf2Dwrite(0, surface_ref_2, x*sizeof(int), y);
        }
    }
    else
    	printf("%d %d FAIL - neznamy typ %d 3\n",threadIdx.y,threadIdx.x,tmp); 
    */
    
	
}

/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// kernel na spocteni 1 iterace langtonovy smycky

__global__ void kernel_langton_2d_surface ()
{
	const int x = (blockIdx.x * blockDim.x) + threadIdx.x;	//calculate the adress from all the block/thread ids 
	const int y = (blockIdx.y * blockDim.y) + threadIdx.y;
	
	int N, S, W, E, C;	
	int address;

    // 12 9 6 3 0    - o kolik bitu se shiftuje doleva
	// C  N E S W C

	surf2Dread(&C, surface_ref, (x)*sizeof(int), y, cudaBoundaryModeZero);          // stred - center
	C = C << 12;

	surf2Dread(&N, surface_ref, (x)*sizeof(int), y-1, cudaBoundaryModeZero);		// nahore - north
	N = N << 9;

	surf2Dread(&E, surface_ref, (x+1)*sizeof(int), y, cudaBoundaryModeZero);        // vlevo - east
	E = E << 6;
	
	surf2Dread(&S, surface_ref, (x)*sizeof(int), y+1, cudaBoundaryModeZero);		// dole - south
	S = S << 3;

	surf2Dread(&W, surface_ref, (x-1)*sizeof(int), y, cudaBoundaryModeZero);        // vpravo -	west

	// adresa do tabulky v texturni pameti
	address = C | N | E | S | W;
	
	// nova hodnota centrlni bunky
	int out = tex1Dfetch(tex_langton_ref, address);
	surf2Dwrite(out, surface_ref_2, x*sizeof(int), y);
    
}


// funkce zaobaluje potrebne pripravy a volani vsech iteraci 1 kernelu
void iteration_function_kernel_2d(int *matrix_a, int *matrix_b, int *langton_table)
{	
	// tabulka s pravidlama
	int* langton_device;
	if (langton)
	{		
		cudaMalloc((void**) &langton_device, LANGTON_TABLE_SIZE *sizeof(int));	//allocate the matrixes on the GPU memory
		cudaMemcpy(langton_device, langton_table, LANGTON_TABLE_SIZE *sizeof(int), cudaMemcpyHostToDevice);		
		cudaBindTexture(NULL, tex_langton_ref, langton_device, LANGTON_TABLE_SIZE *sizeof(int));
	}


	/// priprava surface ------------------------------------------------
	cudaArray* cuda_array_matrix; 				
	cudaArray* cuda_array_matrix_2;
	cudaChannelFormatDesc channel;
	cudaChannelFormatDesc channel_2;

	// vstupni surface
	channel=cudaCreateChannelDesc<int>();	
	cudaMallocArray(&cuda_array_matrix, &channel, sizeof(int)*SIZE_X , SIZE_Y,cudaArraySurfaceLoadStore);
	cudaMemcpy2DToArray(cuda_array_matrix,0,0,matrix_a,sizeof(int)*SIZE_X,sizeof(int)*SIZE_X,SIZE_Y,cudaMemcpyHostToDevice );		
	cudaBindSurfaceToArray(surface_ref, cuda_array_matrix, channel);

	// pomocny surface
	channel_2=cudaCreateChannelDesc<int>();	
	cudaMallocArray(&cuda_array_matrix_2, &channel_2, sizeof(int)*SIZE_X , SIZE_Y,cudaArraySurfaceLoadStore);
	cudaMemcpy2DToArray(cuda_array_matrix_2,0,0,matrix_b,sizeof(int)*SIZE_X,sizeof(int)*SIZE_X,SIZE_Y,cudaMemcpyHostToDevice );		
	cudaBindSurfaceToArray(surface_ref_2, cuda_array_matrix_2, channel_2);


	/// zacatek mereni casu ---------------------------------------------
	float exec_time=0.0f;
	cudaEvent_t beginEvent;
	cudaEvent_t endEvent;
	cudaEventCreate( &beginEvent );
	cudaEventCreate( &endEvent );
	cudaEventRecord( beginEvent, 0 );
	cudaProfilerStart();
	
	/// parametry spousteni ---------------------------------------------
	cudaFuncSetCacheConfig(kernel_life_2d_surface,cudaFuncCachePreferL1);  // preferujeme L1 cache nad sdilenou pameti
	cudaFuncSetCacheConfig(kernel_langton_2d_surface,cudaFuncCachePreferL1);  // preferujeme L1 cache nad sdilenou pameti
	dim3 dimBlock(DIM_BLOCK_X, DIM_BLOCK_Y);	              // pocty threadu v bloku
	dim3 dimGrid(SIZE_X/DIM_BLOCK_X, SIZE_Y/DIM_BLOCK_Y);     // pocty bloku v jednotlivych dimenzich

	for (int i=0; i<ITERATIONS; i++)
	{	
		
		/// zavola se kernel ----------------------------------------------------------------
		if (convey) kernel_life_2d_surface <<<dimGrid, dimBlock>>> ();
		if (langton) kernel_langton_2d_surface <<<dimGrid, dimBlock>>> ();
		cudaDeviceSynchronize();
		
		/// vymeni se pointery -------------------------------------------------------------
		
		if ( i%2==0 )
		{
			cudaBindSurfaceToArray(surface_ref, cuda_array_matrix_2, channel);
			cudaBindSurfaceToArray(surface_ref_2, cuda_array_matrix, channel);
		}
		else
		{
			cudaBindSurfaceToArray(surface_ref, cuda_array_matrix, channel);
			cudaBindSurfaceToArray(surface_ref_2, cuda_array_matrix_2, channel);
		}
				
		cudaDeviceSynchronize();		
	}

	/// konec mereni casu --------------------------------------------------------------
	cudaProfilerStop();
	cudaEventRecord( endEvent, 0 );
	cudaEventSynchronize( endEvent );
	cudaEventElapsedTime( &exec_time, beginEvent, endEvent );
	cudaEventDestroy( beginEvent );
	cudaEventDestroy( endEvent );
	cout << "velikost problemu " << SIZE_X << " x " << SIZE_Y << endl;
	cout << "cas vykonani: " << exec_time << "ms" << endl;
	cout << "iteraci: " << ITERATIONS << endl;
	cout << "cas jedne iterace: " << exec_time/ITERATIONS << "ms" << endl;
	


	// vykopirovani zpet do hostu -------------------------------- 
	cudaMemcpy2DFromArray(matrix_b,sizeof(int)*SIZE_X,cuda_array_matrix_2,0,0,sizeof(int)*SIZE_X,SIZE_Y,cudaMemcpyDeviceToHost );		
	cudaMemcpy2DFromArray(matrix_a,sizeof(int)*SIZE_X,cuda_array_matrix  ,0,0,sizeof(int)*SIZE_X,SIZE_Y,cudaMemcpyDeviceToHost );

	// free -------------------------------------------------------
	cudaFreeArray(cuda_array_matrix);		 
	cudaFreeArray(cuda_array_matrix_2);	

	if (langton)
	{
		cudaUnbindTexture(tex_langton_ref);
		cudaFree(langton_device);	
	}

}

void print_matrix(int *matrix)
{
	for (int i=0; i<40; i++)
	{
		for (int j=0; j<40; j++)
		{
			int a=matrix[i*SIZE_X + j];
			if (a!=0)
				cout << a << " ";
			else
				cout << "  ";
		}
		cout << endl;
	}
}


// prida langtonovu smycku do matice
void add_langton_loop(int *matrix, int basex, int basey)
{
	for (int i=1; i<9; i++) matrix[basey*SIZE_X + basex + i] = 2;  // nahore
	for (int i=1; i<7; i++) matrix[(basey+i)*SIZE_X + basex +9] = 2; // vpravo
	for (int i=1; i<9; i++) matrix[(basey+i)*SIZE_X + basex] = 2;  // vlevo
	for (int i=1; i<14; i++) matrix[(basey+9)*SIZE_X + basex + i] = 2; // dole

	for (int i=2; i<8; i++) matrix[(basey+2)*SIZE_X + basex + i] = 2;		// vnitrni ctverec - nahore
	for (int i=2; i<8; i++) matrix[(basey+i)*SIZE_X + basex+2] = 2;  // vlevo
	for (int i=2; i<8; i++) matrix[(basey+i)*SIZE_X + basex+7] = 2; // vpravo
	for (int i=2; i<14; i++) matrix[(basey+7)*SIZE_X + basex + i] = 2; // dole

	for (int i=2; i<8; i++) matrix[(basey+i)*SIZE_X + basex+8] = 1; // vpravo
	for (int i=9; i<14; i++) matrix[(basey+8)*SIZE_X + basex + i] = 1; // dole


	// leva vypln
	matrix[(basey+1)*SIZE_X + basex + 1] = 1;
	matrix[(basey+2)*SIZE_X + basex + 1] = 0;
	matrix[(basey+3)*SIZE_X + basex + 1] = 7;
	matrix[(basey+4)*SIZE_X + basex + 1] = 1;
	matrix[(basey+5)*SIZE_X + basex + 1] = 0;
	matrix[(basey+6)*SIZE_X + basex + 1] = 7;
	matrix[(basey+7)*SIZE_X + basex + 1] = 1;
	matrix[(basey+8)*SIZE_X + basex + 1] = 0;

	// prava vypln
	matrix[(basey+2)*SIZE_X + basex + 8] = 0;

	// horni vypln
	matrix[(basey+1)*SIZE_X + basex + 1] = 1;
	matrix[(basey+1)*SIZE_X + basex + 2] = 7;		
	matrix[(basey+1)*SIZE_X + basex + 3] = 0;
	matrix[(basey+1)*SIZE_X + basex + 4] = 1;		
	matrix[(basey+1)*SIZE_X + basex + 5] = 4;
	matrix[(basey+1)*SIZE_X + basex + 6] = 0;		
	matrix[(basey+1)*SIZE_X + basex + 7] = 1;
	matrix[(basey+1)*SIZE_X + basex + 8] = 4;

	//dolni vypln
	matrix[(basey+8)*SIZE_X + basex + 1] = 0;
	matrix[(basey+8)*SIZE_X + basex + 2] = 7;		
	matrix[(basey+8)*SIZE_X + basex + 3] = 1;
	matrix[(basey+8)*SIZE_X + basex + 4] = 0;		
	matrix[(basey+8)*SIZE_X + basex + 5] = 7;
	matrix[(basey+8)*SIZE_X + basex + 6] = 1;		
	matrix[(basey+8)*SIZE_X + basex + 7] = 0;
	matrix[(basey+8)*SIZE_X + basex + 8] = 7;
	matrix[(basey+8)*SIZE_X + basex + 14] = 2;		
}

int main(int argc, char *argv[])
{		
	
	/// kontrola parametru -----------------------------------------------------------
	int c;

	if (argc != 2)
	{
		cerr << "./ca_gpu -C       runs Conveys Game of Life" << endl << "./ca_gpu -L       runs Langtons loop" << endl;
		exit (EXIT_FAILURE);
	}

	while ((c = getopt (argc, argv, "CL")) != -1)
	{
		switch (c)
		{
			case 'C': {
				convey = true;
				break;
			}
			case 'L': {
				langton = true;
				break;
			}
			default:{
				cout << "default" << endl;
				exit(1);
			}
		}
	}	

	// alokace a iniciazace ---------------------------------------------------
	int *matrix_a;			//imput matrix
	int *matrix_b;			//output matrix
	cudaHostAlloc((void**) &matrix_a, (SIZE_Y * SIZE_X*sizeof(int)), cudaHostAllocDefault);
	cudaHostAlloc((void**) &matrix_b, (SIZE_Y * SIZE_X*sizeof(int)), cudaHostAllocDefault);


	for(int i=0; i<(SIZE_Y * SIZE_X); i++)
	{	
		matrix_a[i]=0;
		matrix_b[i]=0;		
	}


	// life test - glider
	if (convey)
	{
		matrix_a[4*SIZE_X + 2]=1;
		matrix_a[4*SIZE_X + 3]=1;
		matrix_a[4*SIZE_X + 4]=1;
		matrix_a[3*SIZE_X + 4]=1;
		matrix_a[2*SIZE_X + 3]=1;
	}
	

	int * langton_table = NULL;
	if (langton)
	{
		add_langton_loop(matrix_a, 10, 10);
		add_langton_loop(matrix_a, 300, 500);
		add_langton_loop(matrix_a, 200, 400);
		add_langton_loop(matrix_a, 100, 600);
		add_langton_loop(matrix_a, 500, 800);
		add_langton_loop(matrix_a, 500, 500);
		add_langton_loop(matrix_a, 200, 900);
		add_langton_loop(matrix_a, 700, 300);
		add_langton_loop(matrix_a, 200, 200);
		add_langton_loop(matrix_a, 100, 300);

		int * langton_table = new int[LANGTON_TABLE_SIZE];
		load_langton_tables(langton_table);
	}

	/*
	cout << "input" << endl;
	print_matrix(matrix_a);
	cout << endl;
	*/

	/// samotny kernel ---------------------------------	
	iteration_function_kernel_2d(matrix_a, matrix_b,langton_table);

	/*
	/// vypisy vystupu ---------------------------------
	if (ITERATIONS%2==0)	
	{
		cout << "output" << endl;
		print_matrix(matrix_a);
	}
	else
	{
		cout << "output" << endl;
		print_matrix(matrix_b);
	}
	*/
	



	return EXIT_SUCCESS;	
}
