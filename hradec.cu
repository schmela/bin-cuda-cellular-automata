#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include <cstdlib>

const int	ITERATIONS = 1000;		//ammount of itteration

int 	SIZE_X;		//neither of these values should be set lower than 5 
int 	SIZE_Y;		//for this code to work properly
int		SIZE_Z;		

const int 	DIM_BLOCK_X = 8;	//size of block when running running kernel
const int 	DIM_BLOCK_Y = 8;
const int 	DIM_BLOCK_Z = 8;

const int ORDINARY=1;	//used in "iteration function kernel 3d" to call different kernels
const int TEXTURE=4;
const int TEXTURE_3D=5;
const int SURFACE=6;

texture<float, 1, cudaReadModeElementType> tex_ref;			//jednorozmerna textura, max 128*1024 hodnot
texture<float, 3, cudaReadModeElementType> tex_ref_3d;		// max 4000^3 hodnot
surface<void, 3> surface_ref;
surface<void, 3> surface_ref_2;


/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// Heat distribution KERNEL (based on RK4) - The simple most basic kernel
//  basicly the same as "runge_kutta_4_ret_ite" without the for cycles

//vobycejnej naivni kernel
__global__ void kernel_rk4_3d (float *input, float *output, int SIZE_XP, int SIZE_YP)
{
	const int x = (blockIdx.x * blockDim.x) + threadIdx.x;	//calculate the adress from all the block/thread ids
	const int y = (blockIdx.y * blockDim.y) + threadIdx.y;
	const int z = (blockIdx.z * blockDim.z) + threadIdx.z;
	const int adr = (z+2)*SIZE_YP*SIZE_XP + (y+2)*SIZE_XP + x+2;
		
	output[adr] = input[adr] + (float)STABILITY *
		(
			(( -1.0f/12.0f) * input[adr - 2*SIZE_YP*SIZE_XP])+	// Z axis											 
			(( 16.0f/12.0f) * input[adr - 1*SIZE_YP*SIZE_XP])+ 
			((-30.0f/12.0f) * input[adr])+ 
			(( 16.0f/12.0f) * input[adr + 1*SIZE_YP*SIZE_XP])+ 
			(( -1.0f/12.0f) * input[adr + 2*SIZE_YP*SIZE_XP])+
			
			(( -1.0f/12.0f) * input[adr - 2*SIZE_XP])+	// Y axis											 
			(( 16.0f/12.0f) * input[adr - 1*SIZE_XP])+ 
			((-30.0f/12.0f) * input[adr])+ 
			(( 16.0f/12.0f) * input[adr + 1*SIZE_XP])+ 
			(( -1.0f/12.0f) * input[adr + 2*SIZE_XP])+ 

			(( -1.0f/12.0f) * input[adr - 2])+	// X axis
			(( 16.0f/12.0f) * input[adr - 1])+ 
			((-30.0f/12.0f) * input[adr])+ 
			(( 16.0f/12.0f) * input[adr + 1])+ 
			(( -1.0f/12.0f) * input[adr + 2])
		);		
}//end of kernel
/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// Heat distribution KERNEL (based on RK4) - with texture memory (binds linear array so max size of matrix is 128*1024 of points)
//  basicly the same as "runge_kutta_4_ret_ite" without the for cycles

// v tomto kernelu mam namapovane jednorozmerne pole na texturu; 
// je to kurevsky jednoduchy (1 prikaz) a rychly, ale muzes namapovat maximalne pole o velikosti 128K hodnot
__global__ void kernel_rk4_3d_texture (float *output, int SIZE_XP, int SIZE_YP)
{
	const int x = (blockIdx.x * blockDim.x) + threadIdx.x;	//calculate the adress from all the block/thread ids
	const int y = (blockIdx.y * blockDim.y) + threadIdx.y;
	const int z = (blockIdx.z * blockDim.z) + threadIdx.z;
	const int adr = (z+2)*SIZE_YP*SIZE_XP + (y+2)*SIZE_XP + x+2;
		
	output[adr] = tex1Dfetch (tex_ref, adr) + (float)STABILITY *
		(
			(( -1.0f/12.0f) * tex1Dfetch (tex_ref, adr - 2*SIZE_YP*SIZE_XP))+	// Z axis											 
			(( 16.0f/12.0f) * tex1Dfetch (tex_ref, adr - 1*SIZE_YP*SIZE_XP))+ 
			((-30.0f/12.0f) * tex1Dfetch (tex_ref, adr))+ 
			(( 16.0f/12.0f) * tex1Dfetch (tex_ref, adr + 1*SIZE_YP*SIZE_XP))+ 
			(( -1.0f/12.0f) * tex1Dfetch (tex_ref, adr + 2*SIZE_YP*SIZE_XP))+
			
			(( -1.0f/12.0f) * tex1Dfetch (tex_ref, adr - 2*SIZE_XP))+	// Y axis											 
			(( 16.0f/12.0f) * tex1Dfetch (tex_ref, adr - 1*SIZE_XP))+ 
			((-30.0f/12.0f) * tex1Dfetch (tex_ref, adr))+ 
			(( 16.0f/12.0f) * tex1Dfetch (tex_ref, adr + 1*SIZE_XP))+ 
			(( -1.0f/12.0f) * tex1Dfetch (tex_ref, adr + 2*SIZE_XP))+ 

			(( -1.0f/12.0f) * tex1Dfetch (tex_ref, adr - 2))+	// X axis
			(( 16.0f/12.0f) * tex1Dfetch (tex_ref, adr - 1))+ 
			((-30.0f/12.0f) * tex1Dfetch (tex_ref, adr))+ 
			(( 16.0f/12.0f) * tex1Dfetch (tex_ref, adr + 1))+ 
			(( -1.0f/12.0f) * tex1Dfetch (tex_ref, adr + 2))
		);		
}//end of kernel


/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// Heat distribution KERNEL (based on RK4) - with texture memory (using cudaArray to bind to texture so max size is 4096^3 [Cuda 2.0+])
//  basicly the same as "runge_kutta_4_ret_ite" without the for cycles

// v tomto kernelu pouzivam 3d texturu namapovanou na cudaArray
// vyhodou je ze muzes pouzit pole velikosti 4000^3
// nahovno je, ze ale musis mezi iteracemi zkopirovat output matici do cudaArray (zpomaluje to jako kráva - az 3x pomalejsi)
__global__ void kernel_rk4_3d_texture_3d (float *output, int SIZE_XP, int SIZE_YP)
{
	const int x = (blockIdx.x * blockDim.x) + threadIdx.x + 2;	//calculate the adress from all the block/thread ids
	const int y = (blockIdx.y * blockDim.y) + threadIdx.y + 2;
	const int z = (blockIdx.z * blockDim.z) + threadIdx.z + 2;
	const int adr = (z)*SIZE_YP*SIZE_XP + (y)*SIZE_XP + x;
		
	output[adr] = tex3D (tex_ref_3d, x,y,z) + (float)STABILITY *
		(
			(( -1.0f/12.0f) * tex3D (tex_ref_3d, x,y,z-2)) +	// Z axis											 
			(( 16.0f/12.0f) * tex3D (tex_ref_3d, x,y,z-1)) + 
			((-30.0f/12.0f) * tex3D (tex_ref_3d, x,y,z))   + 
			(( 16.0f/12.0f) * tex3D (tex_ref_3d, x,y,z+1)) + 
			(( -1.0f/12.0f) * tex3D (tex_ref_3d, x,y,z+2)) +
			
			(( -1.0f/12.0f) * tex3D (tex_ref_3d, x,y-2,z)) +	// Y axis											 
			(( 16.0f/12.0f) * tex3D (tex_ref_3d, x,y-1,z)) + 
			((-30.0f/12.0f) * tex3D (tex_ref_3d, x,y,z))   + 
			(( 16.0f/12.0f) * tex3D (tex_ref_3d, x,y+1,z)) + 
			(( -1.0f/12.0f) * tex3D (tex_ref_3d, x,y+2,z)) + 

			(( -1.0f/12.0f) * tex3D (tex_ref_3d, x-2,y,z)) + // X axis
			(( 16.0f/12.0f) * tex3D (tex_ref_3d, x-1,y,z)) +
			((-30.0f/12.0f) * tex3D (tex_ref_3d, x,y,z))   +
			(( 16.0f/12.0f) * tex3D (tex_ref_3d, x+1,y,z)) +
			(( -1.0f/12.0f) * tex3D (tex_ref_3d, x+2,y,z)) 
		);		
}//end of kernel


/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// Heat distribution KERNEL (based on RK4) - using surface

// v tomhle kernelu vubec nepouzivam texturni pamet (implicitne, pac surface vlastne je textura)
// misto matic v globalni pameti mam jen dva surface

//pokud to budes chtit prelozit, tak musis mit pridat parametr "-arch=sm_20" do makefile (nvcc -arch=sm_20 ...)
__global__ void kernel_rk4_3d_surface (int SIZE_XP, int SIZE_YP)
{
	const int x = (blockIdx.x * blockDim.x) + threadIdx.x + 2;	//calculate the adress from all the block/thread ids
	const int y = (blockIdx.y * blockDim.y) + threadIdx.y + 2;
	const int z = (blockIdx.z * blockDim.z) + threadIdx.z + 2;
		
	float tmp, value;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z-2);		//z axis
	value = (-1.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z-1);
	value +=( 16.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z);
	value +=3 * (-30.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z+1);
	value +=( 16.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z+2);
	value +=(-1.0f/12.0f) * tmp;
	
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y-2, z);		//y axis
	value+=(-1.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y-1, z);
	value+=(16.0f/12.0f) * tmp;
	//surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z);
	//value+=(-30.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y+1, z);
	value+=(16.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y+2, z);
	value+=(-1.0f/12.0f) * tmp;
	
	surf3Dread(&tmp, surface_ref, (x-2)*sizeof(float), y, z);	//x axis
	value+=(-1.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x-1)*sizeof(float), y, z);
	value+=(16.0f/12.0f) * tmp;
	//surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z);
	//value+=(-30.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x+1)*sizeof(float), y, z);
	value+=(16.0f/12.0f) * tmp;
	surf3Dread(&tmp, surface_ref, (x+2)*sizeof(float), y, z);
	value+=(-1.0f/12.0f) * tmp;
	
	value=value*(float)STABILITY;
	surf3Dread(&tmp, surface_ref, (x)*sizeof(float), y, z);
	value+=tmp;
	
	surf3Dwrite(value, surface_ref_2, x*sizeof(float), y, z);
}//end of kernel



/// --------------------------------------------------------------------------------------------------------------------------------------------------
/// Wrapper functionfor the KERNELS
//  beware!!!!! 200+ lines of shitty code
void iteration_function_kernel_3d(float *matrix_a, float *matrix_b, int TYPE)
{	
	float *matrix_a_device, *matrix_b_device;	//pointers to the matrixes in GPU memory
	int size=SIZE_Z*SIZE_X*SIZE_Y*sizeof(float);	//size of those matrixes
	
	cudaArray* cuda_array_matrix; 				//cudaArray used to bind to texture or surface
	cudaArray* cuda_array_matrix_2;
	cudaChannelFormatDesc channel;				//channel descriptor for cudaArrays
	cudaChannelFormatDesc channel_2;
	cudaMemcpy3DParms memcpyParms={0};			//used when copying from array to cudaArray
	cudaMemcpy3DParms memcpyParms_2={0};
	cudaExtent extent;							//used when allocating cudaArray (basicly size)
	cudaExtent extent_2;			
	
	if (TYPE!=SURFACE) //allocate the matrixes on the GPU memory and copy values from host memory
	{
		cudaMalloc((void**) &matrix_a_device, size);	//allocate the matrixes on the GPU memory
		cudaMalloc((void**) &matrix_b_device, size);
		cudaMemset(matrix_a_device, 0, size);		//set all the values in device matrixes to 0
		cudaMemset(matrix_b_device, 0, size);
		cudaMemcpy(matrix_b_device, matrix_b, size, cudaMemcpyHostToDevice);	//copy values from host to deviice global memory
		cudaMemcpy(matrix_a_device, matrix_a, size, cudaMemcpyHostToDevice);		
	}
	
	//when using texture memory, its needed to bind the texture reference to appropriate matrix (this works only for small matrixes [~128 000 points])
	if (TYPE==TEXTURE) cudaBindTexture(NULL, tex_ref, matrix_a_device, size);
	
	//3D texture - allocate cudaArray and bind it to 3D texture
	if (TYPE==TEXTURE_3D) 
	{
		extent=make_cudaExtent(SIZE_X, SIZE_Y, SIZE_Z);		//set size of cudaArray
		channel=cudaCreateChannelDesc<float>();		//set chanel descriptor (basicly type?)
		if (cudaMalloc3DArray(&cuda_array_matrix,&channel,extent) == cudaErrorMemoryAllocation) fprintf(stderr, "Error during cudaMalloc3DArray");	//allocate cudaArray
		memcpyParms.kind = cudaMemcpyDeviceToDevice;	//sets memcopy params to copy from matrix_a_device (stored in device memory)...
		memcpyParms.extent = extent;
		memcpyParms.dstArray = cuda_array_matrix;		
		memcpyParms.srcPtr = make_cudaPitchedPtr( (void*)matrix_a_device,sizeof(float)*SIZE_X, SIZE_Y, SIZE_Z);	//... to the cudaArray
		cudaMemcpy3D(&memcpyParms);		
		
		tex_ref_3d.addressMode[0]=cudaAddressModeClamp;		//set texture mode
		tex_ref_3d.addressMode[1]=cudaAddressModeClamp;
		tex_ref_3d.addressMode[2]=cudaAddressModeClamp;
		tex_ref_3d.filterMode=cudaFilterModePoint;        // no interpolation
		tex_ref_3d.normalized = false;
		
		cudaBindTextureToArray(tex_ref_3d,cuda_array_matrix,channel);	//bind the texture to the cudaArray
	}
	/// prepare surface ----------------------------------------------------------------------------------------------------------------------------------------------
	//Surface - use 2 surface instead of 1 texture and 2 matrixes in global memory
	if (TYPE==SURFACE)
	{
		extent_2=make_cudaExtent(SIZE_X, SIZE_Y, SIZE_Z);	//basicly the same code as binding the texture to cudaArray two times...
		channel_2=cudaCreateChannelDesc<float>();	
		if (cudaMalloc3DArray(&cuda_array_matrix_2,&channel_2,extent_2, cudaArraySurfaceLoadStore) == cudaErrorMemoryAllocation) fprintf(stderr, "Error during cudaMalloc3DArray");			
		memcpyParms_2.kind = cudaMemcpyHostToDevice;	//...expect we copy directly from host memory
		memcpyParms_2.extent = extent_2;
		memcpyParms_2.dstArray = cuda_array_matrix_2;
		memcpyParms_2.srcPtr = make_cudaPitchedPtr( (void*)matrix_b,sizeof(float)*SIZE_X, SIZE_Y, SIZE_Z);
		cudaMemcpy3D(&memcpyParms_2);	
				
		cudaBindSurfaceToArray(surface_ref_2, cuda_array_matrix_2, channel); //here we bind surface to cudaArray
		
		extent=make_cudaExtent(SIZE_X, SIZE_Y, SIZE_Z);	//and here do do the came thing with second surface
		channel=cudaCreateChannelDesc<float>();
		if (cudaMalloc3DArray(&cuda_array_matrix,&channel,extent,cudaArraySurfaceLoadStore) == cudaErrorMemoryAllocation) fprintf(stderr, "Error during cudaMalloc3DArray");			
		memcpyParms.kind = cudaMemcpyHostToDevice;
		memcpyParms.extent = extent;
		memcpyParms.dstArray = cuda_array_matrix;
		memcpyParms.srcPtr = make_cudaPitchedPtr( (void*)matrix_a,sizeof(float)*SIZE_X, SIZE_Y, SIZE_Z);
		cudaMemcpy3D(&memcpyParms);		
		
		cudaBindSurfaceToArray(surface_ref, cuda_array_matrix, channel);
	}
	
	for (int i=0; i<ITERATIONS; i++)
	{	
		dim3 dimBlock(DIM_BLOCK_X, DIM_BLOCK_Y, DIM_BLOCK_Z);		//set size of block and grid
		dim3 dimGrid((SIZE_X-4)/DIM_BLOCK_X, (SIZE_Y-4)/DIM_BLOCK_Y, (SIZE_Z-4)/DIM_BLOCK_Z);
		
		///call the propper kernel (based on parameter "TYPE") ---------------------------------------------------------------------------------------------------------
		if (TYPE==ORDINARY) kernel_rk4_3d <<<dimGrid, dimBlock>>> (matrix_a_device, matrix_b_device, SIZE_X, SIZE_Y);
		else if (TYPE==TEXTURE) kernel_rk4_3d_texture <<<dimGrid, dimBlock>>> (matrix_b_device, SIZE_X, SIZE_Y);
		else if (TYPE==TEXTURE_3D) kernel_rk4_3d_texture_3d <<<dimGrid, dimBlock>>> (matrix_b_device, SIZE_X, SIZE_Y);
		else if (TYPE==SURFACE) kernel_rk4_3d_surface <<<dimGrid, dimBlock>>> ( SIZE_X, SIZE_Y);
		cudaDeviceSynchronize();
		
		/// exchange pointers --------------------------------------------------------------------------------------------------------------------------------------------
		
		if (TYPE==TEXTURE) cudaUnbindTexture(tex_ref);		//unbind texture before exchanging pointers
		else if (TYPE==TEXTURE_3D) cudaUnbindTexture(tex_ref_3d);	//same with 3D texture
		
		float *tmp=matrix_a_device;			//exchange pointers to output and input matrix
		matrix_a_device=matrix_b_device;
		matrix_b_device=tmp;
		
		if (TYPE==TEXTURE) cudaBindTexture(NULL, tex_ref, matrix_a_device, size);	//bind texture after the pointers were exchanged			
		else if (TYPE==TEXTURE_3D)	//with 3D matrix we have to copy values from one matrix to cudaArray and then bind that array to the texture
		{
			memcpyParms.srcPtr = make_cudaPitchedPtr( (void*)matrix_a_device,sizeof(float)*SIZE_X, SIZE_Y, SIZE_Z);
			cudaMemcpy3D(&memcpyParms);		//copy values from 
			cudaBindTextureToArray(tex_ref_3d,cuda_array_matrix,channel);
		}
		
		if ( (TYPE==SURFACE)&&(i%2==0) )	//when using surfaces, exchange surface pointers
		{
			cudaBindSurfaceToArray(surface_ref, cuda_array_matrix_2, channel);
			cudaBindSurfaceToArray(surface_ref_2, cuda_array_matrix, channel);
		}
		else if ( (TYPE==SURFACE)&&(i%2==1) )
		{
			cudaBindSurfaceToArray(surface_ref, cuda_array_matrix, channel);
			cudaBindSurfaceToArray(surface_ref_2, cuda_array_matrix_2, channel);
		}
		
		
		cudaDeviceSynchronize();		
	}
	///free all used resources -------------------------------------------------------------------------------------------------------------------------------------------------
	
	if (TYPE==TEXTURE) cudaUnbindTexture(tex_ref);	// unbinds texture
	if (TYPE==TEXTURE_3D)	// free array and unbind 3D texture
	{
		 cudaUnbindTexture(tex_ref_3d);
		 cudaFreeArray(cuda_array_matrix);		 
	}
	if (TYPE==SURFACE)	// copy results from cudaArray (used for surfaces) to host memory and free said arrays
	{
		cudaMemcpy3DParms memcpyParms_3={0};
		memcpyParms_3.kind = cudaMemcpyDeviceToHost;
		memcpyParms_3.extent = make_cudaExtent(SIZE_X, SIZE_Y, SIZE_Z);
		memcpyParms_3.dstPtr = make_cudaPitchedPtr( (void*)matrix_b,sizeof(float)*SIZE_X, SIZE_Y, SIZE_Z);
		memcpyParms_3.srcArray = cuda_array_matrix_2;
		cudaMemcpy3D(&memcpyParms_3);
		
		cudaMemcpy3DParms memcpyParms_4={0};
		memcpyParms_4.kind = cudaMemcpyDeviceToHost;
		memcpyParms_4.extent = make_cudaExtent(SIZE_X, SIZE_Y, SIZE_Z);
		memcpyParms_4.dstPtr = make_cudaPitchedPtr( (void*)matrix_a,sizeof(float)*SIZE_X, SIZE_Y, SIZE_Z);
		memcpyParms_4.srcArray = cuda_array_matrix;
		cudaMemcpy3D(&memcpyParms_4);	
		
		cudaFreeArray(cuda_array_matrix);		 
		cudaFreeArray(cuda_array_matrix_2);		
	}
	else 		// kernel using surface doesnt work with matrixes allocated in device memory (it uses its own cudaArray)
	{			// copy results from matrixes in device global memory to host memory
		cudaMemcpy(matrix_a, matrix_a_device, size, cudaMemcpyDeviceToHost);
		cudaMemcpy(matrix_b, matrix_b_device, size, cudaMemcpyDeviceToHost);
	}
	cudaFree(matrix_a_device);	//and free the memory in device
	cudaFree(matrix_b_device);
	cudaFree(point_vector_device);	
}





int main ()
{
	SIZE_X=8		* DIM_BLOCK_X	+ 4;			//size of tested matrix
	SIZE_Y=8	   	* DIM_BLOCK_Y 	+ 4;			
	SIZE_Z=8	   	* DIM_BLOCK_Z 	+ 4;			
	
	
	//tady my se mely naalokovat a inicializovat pole
	float *matrix_a;			//imput matrix
	float *matrix_b;			//output matrix
	temp1 = cudaHostAlloc((void**) &matrix_a, (SIZE_Z * SIZE_Y * SIZE_X*sizeof(float)), cudaHostAllocDefault);
	temp2 = cudaHostAlloc((void**) &matrix_b, (SIZE_Z * SIZE_Y * SIZE_X*sizeof(float)), cudaHostAllocDefault);
	srand(time(0));
	//sets initial value of both matrixes (this is used for testing only)
	for(int i=0; i<(SIZE_Z * SIZE_Y * SIZE_X); i++)
	{	
		float tmp=(rand() % 500)/100.0 + 32.0;	//generate temperatures between 32-37 C
		matrix_a[i]=tmp;
		matrix_b[i]=tmp;		
	}
	
	
	
	
	iteration_function_kernel_3d(matrix_a, matrix_b, ORDINARY);
	iteration_function_kernel_3d(matrix_a, matrix_b, TEXTURE);
	iteration_function_kernel_3d(matrix_a, matrix_b, TEXTURE_3D);
	iteration_function_kernel_3d(matrix_a, matrix_b, SURFACE);
	





}













